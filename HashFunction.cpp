#include <iostream>
#include <string>
#include <iostream>
#define MAX_LEN 100

std::string hashArray[MAX_LEN]; // tentative

int getHash(std::string s)
{
    // A simple hashing, no collision handled
    int sum=0,index=0;
    for(std::string::size_type i=0; i < s.length(); i++)
    {
        sum += s[i];
    }
    index = sum * (sum + 3) % MAX_LEN;
    return index;
}

int main()
{
    std::string text;
    int index;
    std::cout << "Text: " ;
    std::cin >> text;
    index = getHash(text);
    std::cout << "Generated index: " << index << "\n";
    hashArray[index] = text;
    std::cout << "Text in array: " << hashArray[index];
    return 0;
}